package grupo04hackathon.sprintbootpractitionerv2.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import grupo04hackathon.sprintbootpractitionerv2.model.CatalogoTasaModel;
import grupo04hackathon.sprintbootpractitionerv2.service.CatalogoTasaService;

@RestController
@RequestMapping("${url.base}")
public class CatalogoTasaController {

	@Autowired CatalogoTasaService catalogoTasaService;
	
	@GetMapping("/tasas/{idTasa}")
    public ResponseEntity obtenerTasaPorId(@PathVariable String idTasa) {
		Optional<CatalogoTasaModel> tasaModel= catalogoTasaService.obtenerTasaPorId(idTasa);
		if(!tasaModel.isPresent()) {
       	 	return new ResponseEntity<>("Tasa no encontrada", HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(tasaModel.get());
    }
	
	@GetMapping("/tasas")
    public List<CatalogoTasaModel> obtenerTasaPorMontoPlazo(
    		@RequestParam(required = false) Double monto,
    		@RequestParam(required = false) Integer plazo) {
         return catalogoTasaService.obtenerTasaPorMontoPlazo(monto, plazo);
    }

    @PostMapping("/tasas")
    public ResponseEntity agregarTasas(@RequestBody CatalogoTasaModel catalogoTasa){
    	catalogoTasaService.agregarTasas(catalogoTasa);
    	return ResponseEntity.ok(catalogoTasa);
    }
}

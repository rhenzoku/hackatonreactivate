package grupo04hackathon.sprintbootpractitionerv2.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import grupo04hackathon.sprintbootpractitionerv2.model.ClienteModel;
import grupo04hackathon.sprintbootpractitionerv2.service.ClienteService;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping("${url.base}")
public class ClienteController {

    @Autowired
    ClienteService servicioCliente;

    @GetMapping("/clientes")
    public Page<ClienteModel> obtenerClientes(
            @RequestParam (required = false) Integer numPagina,
            @RequestParam (required = false) Integer tamPagina
    ){
        // Controlar parámetros paginación.
        if(numPagina == null || numPagina < 0)
            numPagina = 0;
        if(tamPagina == null || tamPagina < 20)
            tamPagina = 20;
        if(tamPagina > 100)
            tamPagina = 100;
        return this.servicioCliente.obtenerClientes(numPagina, tamPagina);
    }

    @PostMapping("/clientes")
    public ResponseEntity agregarCliente(@RequestBody ClienteModel cliente){
    	ClienteModel clienteModel = servicioCliente.obtenerClientePorDoc(cliente.getTipoDocumento(), cliente.getNroDocumento());
        if (clienteModel == null) {

            //Validar tipo documento
            if (cliente.getTipoDocumento().equals("DNI") || cliente.getTipoDocumento().equals("CE")){
                //Validar numero documento
                if (validarNumeros(cliente.getNroDocumento()) == true) {
                    //Validar nombre
                    if (validarNombre(cliente.getNombreCliente()) == true){
                        //Validar Correo
                        if (validarCorreo(cliente.getCorreo()) == true){
                            //Validar telefono
                            if (validarNumeros(cliente.getTelefono()) == true){
                                servicioCliente.agregarCliente(cliente);
                                return ResponseEntity.ok(cliente);
                            }
                            else
                                return new ResponseEntity<>("Telefono errado.", HttpStatus.BAD_REQUEST);
                        }
                        else
                            return new ResponseEntity<>("Correo errado.", HttpStatus.BAD_REQUEST);
                    }
                    else
                        return new ResponseEntity<>("Nombre errado.", HttpStatus.BAD_REQUEST);
                }
                else
                    return new ResponseEntity<>("Num de documento errado.", HttpStatus.BAD_REQUEST);
            } else
                return new ResponseEntity<>("Tipo de documento errado.", HttpStatus.BAD_REQUEST);

        } else{
            return new ResponseEntity<>("El cliente ya se encuentra registrado.", HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/clientes/")
    public ClienteModel obtenerClientePorDoc(
            @RequestParam(required = true) String tipoDocumento,
            @RequestParam(required = true) String nroDocumento)
    {
        return this.servicioCliente.obtenerClientePorDoc(tipoDocumento,nroDocumento);
    }

    @GetMapping("/clientes/{codCliente}")
    public ClienteModel obtenerCliente(@PathVariable String codCliente) {
        return this.servicioCliente.obtenerCliente(codCliente);
    }
    public boolean validarNumeros(String cadena) {
        if (cadena.matches("[0-9]*")) {
            return true;
        } else
            return false;
    }
    public boolean validarNombre(String cadena) {
        if (cadena.matches("[a-zA-Z]+\\.?")) {
            return true;
        } else
            return false;
    }

    public boolean validarCorreo(String cadena) {
        if (cadena.matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")) {
            return true;
        } else
            return false;
    }

}

package grupo04hackathon.sprintbootpractitionerv2.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import grupo04hackathon.sprintbootpractitionerv2.model.DeudaClienteModel;
import grupo04hackathon.sprintbootpractitionerv2.service.DeudaClienteService;

@RestController
@RequestMapping("${url.base}")
public class ClienteDeudaController {

    @Autowired
    DeudaClienteService deudaClienteService;

    @GetMapping("/clientes/{codCliente}/deudas")
    public ResponseEntity <DeudaClienteModel> obtenerCliente(@PathVariable String codCliente) {
    	DeudaClienteModel dc = deudaClienteService.obtenerDeudaCliente(codCliente);
        if (dc == null) {
            return new ResponseEntity("Deuda del Cliente no encontrada.", HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(dc);
    }
}

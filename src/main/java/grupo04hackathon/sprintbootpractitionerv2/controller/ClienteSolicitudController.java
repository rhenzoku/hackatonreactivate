package grupo04hackathon.sprintbootpractitionerv2.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import grupo04hackathon.sprintbootpractitionerv2.model.CatalogoTasaModel;
import grupo04hackathon.sprintbootpractitionerv2.model.CuotaCronogramaModel;
import grupo04hackathon.sprintbootpractitionerv2.model.DeudaClienteModel;
import grupo04hackathon.sprintbootpractitionerv2.model.SolicitudModel;
import grupo04hackathon.sprintbootpractitionerv2.service.CatalogoTasaService;
import grupo04hackathon.sprintbootpractitionerv2.service.CuotaCronogramaService;
import grupo04hackathon.sprintbootpractitionerv2.service.DeudaClienteService;
import grupo04hackathon.sprintbootpractitionerv2.service.SolicitudService;

@RestController
@RequestMapping("${url.base}")
public class ClienteSolicitudController {
	
    @Autowired
    SolicitudService solicitudService;
    
    @Autowired
    DeudaClienteService deudaClienteService;
    
    @Autowired 
    CatalogoTasaService catalogoTasaService;
    
    @Autowired
    CuotaCronogramaService cuotaCronogramaService;


    @GetMapping("/clientes/{codCliente}/solicitudes" )
    public ResponseEntity getSolicitudesByCodCliente(@PathVariable String codCliente){
        SolicitudModel solicitudModel=solicitudService.findByCodCliente(codCliente);
        if (solicitudModel == null) {
            return new ResponseEntity<>("El cliente no tiene solicitud ingresada.", HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(solicitudModel);
    }

    @PostMapping("/solicitudes")
    public ResponseEntity postSolicitudes(@RequestBody SolicitudModel solicitudModel){
        SolicitudModel oSolicitudModel=solicitudService.findByCodCliente(solicitudModel.getClienteModel().getCodCliente());
        if (oSolicitudModel == null) {
        	DeudaClienteModel dc = deudaClienteService.obtenerDeudaCliente(solicitudModel.getClienteModel().getCodCliente());
        	
        	if(dc == null) {
        		return new ResponseEntity<>("Por el momento no podemos ofrecerle una oferta", HttpStatus.NOT_FOUND);
        	}
        	else if (dc.getDeudaActual() + solicitudModel.getImporteDesembolso() <= dc.getDeudaMaxima()){
        		
        		List<CatalogoTasaModel> listaCatalogo = catalogoTasaService.obtenerTasaPorMontoPlazo(solicitudModel.getImporteDesembolso(), solicitudModel.getPlazoMeses());
        		
        		if(listaCatalogo == null || listaCatalogo.isEmpty()) {
        			return new ResponseEntity<>("Por el momento no podemos ofrecerle una oferta", HttpStatus.NOT_FOUND);
        		}
        		else {
        			solicitudModel.setTasa(listaCatalogo.get(0).getTasa());
        			CuotaCronogramaModel cronograma = cuotaCronogramaService.calcularCronograma(
        					solicitudModel.getTasa(),
        					solicitudModel.getPlazoMeses(),
        					solicitudModel.getImporteDesembolso());
        			
        			if(cronograma == null) {
        				return new ResponseEntity<>("Por el momento no podemos ofrecerle una oferta", HttpStatus.NOT_FOUND);
        			}else {
        				solicitudModel.setCuota(cronograma.getCuota());
        				solicitudModel.setEstado("0");
        				solicitudService.save(solicitudModel);
						solicitudModel=solicitudService.findByCodCliente(solicitudModel.getClienteModel().getCodCliente());
						cronograma.setId(solicitudModel.getId());
						cuotaCronogramaService.save(cronograma);
        				return ResponseEntity.ok(solicitudModel);
        			}
        		}
        	}
        	else {
        		return new ResponseEntity<>("Ha superado el nivel de endeudamiento", HttpStatus.NOT_FOUND);
        	}
        }else{
            return new ResponseEntity<>("El cliente ya tiene una solicitud ingresada, que está en proceso.", HttpStatus.NOT_FOUND);
        }
    }
    
    @PatchMapping("/solicitudes/{idSolicitud}")
    public ResponseEntity cambiarEstadoSolicitud(
    		@PathVariable String idSolicitud,
    		@RequestBody SolicitudModel solicitudModel) {
        try {
        	
        	//Validar si existe la solicitud
        	SolicitudModel solicitud = solicitudService.findById(idSolicitud).get();
            if (solicitud == null) {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "La solicitud no existe");
            }
            
            // Validación de los datos de entrada.
            if(solicitudModel.getTasa() == null || solicitudModel.getTasa() <= 0.0) {
            	return new ResponseEntity<>("La tasa tiene un valor no válido", HttpStatus.BAD_REQUEST);
            }
            if(solicitudModel.getCuota() == null || solicitudModel.getCuota() <= 0.0) {
            	return new ResponseEntity<>("La cuota tiene un valor no válido", HttpStatus.BAD_REQUEST);
            }
            if(solicitudModel.getEstado() == null || solicitudModel.getEstado().trim().isEmpty()) {
            	return new ResponseEntity<>("Estado de la solicitud incorrecto", HttpStatus.BAD_REQUEST);
            }
            solicitudService.emparcharSolicitud(idSolicitud, solicitudModel.getTasa(), solicitudModel.getCuota(), solicitudModel.getEstado());
            return ResponseEntity.ok("Se actualizó correctamente la solicitud");
  
        } catch(Exception x) {
        	return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}

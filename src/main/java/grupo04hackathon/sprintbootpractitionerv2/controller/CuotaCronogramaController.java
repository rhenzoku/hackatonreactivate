package grupo04hackathon.sprintbootpractitionerv2.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import grupo04hackathon.sprintbootpractitionerv2.model.CuotaCronogramaModel;
import grupo04hackathon.sprintbootpractitionerv2.service.CuotaCronogramaService;

@RestController
@RequestMapping("${url.base}")
public class CuotaCronogramaController {

    @Autowired
    CuotaCronogramaService cuotaCronogramaService;

    @GetMapping("/cuotaCronogramas/{id}" )
    public ResponseEntity getCuotaCronogramasById(@PathVariable String id){
        Optional<CuotaCronogramaModel> cuotaCronogramaModel= cuotaCronogramaService.findById(id);
        if (cuotaCronogramaModel == null) {
            return new ResponseEntity<>("No existe cuota cronogramas para el Id " + id, HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(cuotaCronogramaModel);
    }

    @GetMapping("/cuotaCronogramas/")
    public CuotaCronogramaModel getCuotaCronogramasByInfo(
            @RequestParam(required = true) Double tea,
            @RequestParam(required = true) int plazo,
            @RequestParam(required = true) Double importeDesembolsado)
    {
    	CuotaCronogramaModel simulacion = cuotaCronogramaService.calcularCronograma(tea,plazo,importeDesembolsado);
    	simulacion.setId("simulacion");
        return simulacion;
    }

    @PostMapping("/cuotaCronogramas")
    public ResponseEntity postSolicitudes(@RequestBody CuotaCronogramaModel cuotaCronogramaModel){
        Optional<CuotaCronogramaModel> oCuotaCronogramaModel=cuotaCronogramaService.findById(cuotaCronogramaModel.getId());
        if (oCuotaCronogramaModel == null) {
            cuotaCronogramaService.save(cuotaCronogramaModel);
            return ResponseEntity.ok(cuotaCronogramaModel);
        }else{
            return new ResponseEntity<>("Ya se ha registrado la cuota y cronograma.", HttpStatus.NOT_FOUND);
        }
    }


}

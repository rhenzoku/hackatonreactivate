package grupo04hackathon.sprintbootpractitionerv2.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import grupo04hackathon.sprintbootpractitionerv2.model.ClienteModel;
import grupo04hackathon.sprintbootpractitionerv2.model.DeudaClienteModel;
import grupo04hackathon.sprintbootpractitionerv2.service.ClienteService;
import grupo04hackathon.sprintbootpractitionerv2.service.DeudaClienteService;

@RestController
@RequestMapping("${url.base}")
public class DeudaClienteController {

    @Autowired
    DeudaClienteService deudaClienteService;
    
    @Autowired
    ClienteService servicioCliente;

    @PostMapping("/deudaClientes")
    public ResponseEntity<String> agregarDeudaCliente(@RequestBody DeudaClienteModel dc){

        if(dc.getCodCliente() == null || dc.getCodCliente().isEmpty()){
            return new ResponseEntity<>("Campo Codigo Cliente debe ser informado", HttpStatus.CREATED);
        }
        if(dc.getDeudaActual() == null || dc.getDeudaActual() < 0){
            return new ResponseEntity<>("Ingrese una deuda mínima válida", HttpStatus.CREATED);
        }
        if(dc.getDeudaMaxima() == null || dc.getDeudaMaxima() < 0){
            return new ResponseEntity<>("Ingrese una deuda máxima válida", HttpStatus.CREATED);
        }
        
        ClienteModel cliente = servicioCliente.obtenerCliente(dc.getCodCliente());
        if(cliente == null) {
        	return new ResponseEntity<>("El cliente no existe", HttpStatus.BAD_REQUEST);
        }

        DeudaClienteModel dcf = deudaClienteService.obtenerDeudaCliente(dc.getCodCliente());
        if (dcf != null ) {
            return new ResponseEntity<>("Cliente ya  tiene deuda registrada", HttpStatus.BAD_REQUEST);
        }

        this.deudaClienteService.agregarDeudaCliente(dc);
        return new ResponseEntity<>("Deuda cliente registrada correctamente", HttpStatus.CREATED);
    }

    @GetMapping("/deudaClientes")
    public List<DeudaClienteModel> obtenerDeudaClientes(){
        return this.deudaClienteService.obtenerDeudaClientes();
    }

    @GetMapping("/deudaClientes/{codCliente}")
    public ResponseEntity <DeudaClienteModel>obtenerDeudaCliente(@PathVariable String codCliente) {
        System.out.println("ingresa al metodo ");
        DeudaClienteModel dc = deudaClienteService.obtenerDeudaCliente(codCliente);
        if (dc == null) {
            return new ResponseEntity("Deuda del Cliente no encontrada.", HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(dc);
    }
}

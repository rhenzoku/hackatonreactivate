package grupo04hackathon.sprintbootpractitionerv2.controller;

import grupo04hackathon.sprintbootpractitionerv2.model.OfertaModel;
import grupo04hackathon.sprintbootpractitionerv2.service.OfertaModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("${url.base}")
public class OfertaModelController {

    @Autowired
    OfertaModelService ofertaModelService;

    @GetMapping("/ofertas")
    public Page<OfertaModel> obtenerOfertaModels(
            @RequestParam(required = false) Integer nroPagina,
            @RequestParam(required = false) Integer tamPagina
    ) {
        // Controlar parámetros paginación.
        if(nroPagina == null || nroPagina < 0)
            nroPagina = 0;
        if(tamPagina == null || tamPagina < 20)
            tamPagina = 20;
        if(tamPagina > 100)
            tamPagina = 100;

        return this.ofertaModelService.obtenerOfertaModels(nroPagina,tamPagina);
    }
}

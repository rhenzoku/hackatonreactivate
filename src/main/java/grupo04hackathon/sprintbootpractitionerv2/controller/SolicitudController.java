package grupo04hackathon.sprintbootpractitionerv2.controller;

import grupo04hackathon.sprintbootpractitionerv2.model.SolicitudModel;
import grupo04hackathon.sprintbootpractitionerv2.service.SolicitudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("${url.base}")
public class SolicitudController {
    @Autowired
    SolicitudService solicitudService;

    @GetMapping("/solicitudes")
    public List<SolicitudModel> getSolicitudes() {
        return solicitudService.findAll();
    }

    @GetMapping("/solicitudes/{codCliente}" )
    public ResponseEntity getSolicitudesByCodCliente(@PathVariable String codCliente){
        SolicitudModel solicitudModel=solicitudService.findByCodCliente(codCliente);
        if (solicitudModel == null) {
            return new ResponseEntity<>("El cliente no tiene solicitud ingresada.", HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(solicitudModel);
    }

	/*
	 * @PostMapping("/solicitudes") public ResponseEntity
	 * postSolicitudes(@RequestBody SolicitudModel solicitudModel){ SolicitudModel
	 * oSolicitudModel=solicitudService.findByCodCliente(solicitudModel.
	 * getClienteModel().getCodCliente()); if (oSolicitudModel == null) {
	 * solicitudService.save(solicitudModel); return
	 * ResponseEntity.ok(solicitudModel); }else{ return new ResponseEntity<>
	 * ("El cliente ya tiene una solicitud ingresada, que está en proceso.",
	 * HttpStatus.NOT_FOUND); } }
	 */
}

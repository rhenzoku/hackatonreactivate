package grupo04hackathon.sprintbootpractitionerv2.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="tasas")
public class CatalogoTasaModel {
	@Id private String id;
    private Double importeMin;
    private Double importeMax;
    private int plazoMin;
    private int plazoMax;
    private Double tasa;
    
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Double getImporteMin() {
		return importeMin;
	}
	public void setImporteMin(Double importeMin) {
		this.importeMin = importeMin;
	}
	public Double getImporteMax() {
		return importeMax;
	}
	public void setImporteMax(Double importeMax) {
		this.importeMax = importeMax;
	}
	public int getPlazoMin() {
		return plazoMin;
	}
	public void setPlazoMin(int plazoMin) {
		this.plazoMin = plazoMin;
	}
	public int getPlazoMax() {
		return plazoMax;
	}
	public void setPlazoMax(int plazoMax) {
		this.plazoMax = plazoMax;
	}
	public Double getTasa() {
		return tasa;
	}
	public void setTasa(Double tasa) {
		this.tasa = tasa;
	}
    
    
}

package grupo04hackathon.sprintbootpractitionerv2.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Document
public class ClienteModel {

    @Id @JsonProperty(access= JsonProperty.Access.READ_WRITE)
    private String codCliente;
    private String tipoDocumento;
    private String nroDocumento;
    private String nombreCliente;
    private String correo;
    private String telefono;

    public ClienteModel() {

    }

    public ClienteModel(String codCliente, String tipoDocumento, String nroDocumento, String nombreCliente, String correo, String telefono) {
        this.codCliente = codCliente;
        this.tipoDocumento = tipoDocumento;
        this.nroDocumento = nroDocumento;
        this.nombreCliente = nombreCliente;
        this.correo = correo;
        this.telefono = telefono;
    }


    public String getCodCliente() {
        return codCliente;
    }

    public void setCodCliente(String codCliente) {
        this.codCliente = codCliente;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getNroDocumento() {
        return nroDocumento;
    }

    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

}

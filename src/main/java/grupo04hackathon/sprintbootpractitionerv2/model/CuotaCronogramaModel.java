package grupo04hackathon.sprintbootpractitionerv2.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.Id;

import java.util.List;

public class CuotaCronogramaModel {
    private String id;
    private Double cuota;
    private List<CronogramaModel> cronograma;

    public CuotaCronogramaModel() {
    }

    public CuotaCronogramaModel(Double cuota, List<CronogramaModel> cronograma) {
        this.cuota = cuota;
        this.cronograma = cronograma;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Double getCuota() {
        return cuota;
    }

    public void setCuota(Double cuota) {
        this.cuota = cuota;
    }

    public List<CronogramaModel> getCronograma() {
        return cronograma;
    }

    public void setCronograma(List<CronogramaModel> cronograma) {
        this.cronograma = cronograma;
    }


}

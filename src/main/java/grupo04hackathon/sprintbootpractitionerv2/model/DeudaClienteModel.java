package grupo04hackathon.sprintbootpractitionerv2.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class DeudaClienteModel {
    @JsonIgnore
    @Id private String id;
    private String codCliente;
    private Double deudaActual;
    private Double deudaMaxima;

    public DeudaClienteModel() {
    }

    public DeudaClienteModel(String id, String codCliente, Double deudaActual, Double deudaMaxima) {
        this.id = id;
        this.codCliente = codCliente;
        this.deudaActual = deudaActual;
        this.deudaMaxima = deudaMaxima;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCodCliente() {
        return codCliente;
    }

    public void setCodCliente(String codCliente) {
        this.codCliente = codCliente;
    }

    public Double getDeudaActual() {
        return deudaActual;
    }

    public void setDeudaActual(Double deudaActual) {
        this.deudaActual = deudaActual;
    }

    public Double getDeudaMaxima() {
        return deudaMaxima;
    }

    public void setDeudaMaxima(Double deudaMaxima) {
        this.deudaMaxima = deudaMaxima;
    }
}

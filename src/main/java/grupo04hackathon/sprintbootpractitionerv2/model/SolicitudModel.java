package grupo04hackathon.sprintbootpractitionerv2.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;

public class SolicitudModel {
    @Id private String id;
    private ClienteModel clienteModel;
    private Double importeDesembolso;
    private int plazoMeses;
    private String tipoCuota;
    private Double cuota;
    private Double tasa;
    private String estado;//0 Pendiente de aprobar, 1: Aceptado, 2: Rechazado

    public SolicitudModel() {
    }

    public SolicitudModel(String id, ClienteModel clienteModel, Double importeDesembolso, int plazoMeses,
			String tipoCuota, Double cuota, Double tasa, String estado) {
		super();
		this.id = id;
		this.clienteModel = clienteModel;
		this.importeDesembolso = importeDesembolso;
		this.plazoMeses = plazoMeses;
		this.tipoCuota = tipoCuota;
		this.cuota = cuota;
		this.tasa = tasa;
		this.estado = estado;
	}

	public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ClienteModel getClienteModel() {
        return clienteModel;
    }

    public void setClienteModel(ClienteModel clienteModel) {
        this.clienteModel = clienteModel;
    }

    public Double getImporteDesembolso() {
        return importeDesembolso;
    }

    public void setImporteDesembolso(Double importeDesembolso) {
        this.importeDesembolso = importeDesembolso;
    }

    public int getPlazoMeses() {
        return plazoMeses;
    }

    public void setPlazoMeses(int plazoMeses) {
        this.plazoMeses = plazoMeses;
    }

    public String getTipoCuota() {
        return tipoCuota;
    }

    public void setTipoCuota(String tipoCuota) {
        this.tipoCuota = tipoCuota;
    }


    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

	public Double getCuota() {
		return cuota;
	}

	public void setCuota(Double cuota) {
		this.cuota = cuota;
	}

	public Double getTasa() {
		return tasa;
	}

	public void setTasa(Double tasa) {
		this.tasa = tasa;
	}
    

}

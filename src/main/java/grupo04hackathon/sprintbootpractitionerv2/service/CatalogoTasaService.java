package grupo04hackathon.sprintbootpractitionerv2.service;

import java.util.List;
import java.util.Optional;

import grupo04hackathon.sprintbootpractitionerv2.model.CatalogoTasaModel;

public interface CatalogoTasaService {

	public List<CatalogoTasaModel> obtenerTasas();
	public Optional<CatalogoTasaModel> obtenerTasaPorId(String id);
	public List<CatalogoTasaModel> obtenerTasaPorMontoPlazo(Double monto, Integer plazo);
	public String agregarTasas(CatalogoTasaModel catalogoTasa);
}

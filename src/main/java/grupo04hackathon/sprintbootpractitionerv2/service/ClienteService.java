package grupo04hackathon.sprintbootpractitionerv2.service;

import grupo04hackathon.sprintbootpractitionerv2.model.ClienteModel;
import org.springframework.data.domain.Page;

public interface ClienteService {

    public void agregarCliente(ClienteModel cliente);

    public Page<ClienteModel> obtenerClientes(int pagina, int tamPagina);

    public ClienteModel obtenerCliente(String conCliente);

    public ClienteModel obtenerClientePorDoc(String tipoDocumento, String nroDocumento);
}

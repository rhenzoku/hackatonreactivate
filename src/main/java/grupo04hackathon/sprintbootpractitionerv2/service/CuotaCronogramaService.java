package grupo04hackathon.sprintbootpractitionerv2.service;


import grupo04hackathon.sprintbootpractitionerv2.model.CuotaCronogramaModel;

import java.util.List;
import java.util.Optional;

public interface CuotaCronogramaService {

    public List<CuotaCronogramaModel> findAll();

    public Optional<CuotaCronogramaModel>  findById(String id);

    public void save(CuotaCronogramaModel cuotaCronogramaModel);

    public CuotaCronogramaModel calcularCronograma(Double rate,int term,Double financeAmount);
}

package grupo04hackathon.sprintbootpractitionerv2.service;

import grupo04hackathon.sprintbootpractitionerv2.model.DeudaClienteModel;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface DeudaClienteService {

    public String agregarDeudaCliente(DeudaClienteModel dc);
    public List<DeudaClienteModel> obtenerDeudaClientes();
    public DeudaClienteModel obtenerDeudaCliente(String codCliente);

}

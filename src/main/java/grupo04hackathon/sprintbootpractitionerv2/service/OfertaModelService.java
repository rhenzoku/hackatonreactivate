package grupo04hackathon.sprintbootpractitionerv2.service;

import grupo04hackathon.sprintbootpractitionerv2.model.OfertaModel;
import org.springframework.data.domain.Page;

public interface OfertaModelService {
    public Page<OfertaModel> obtenerOfertaModels(int pagina, int tamaniopagina);
}

package grupo04hackathon.sprintbootpractitionerv2.service;

import grupo04hackathon.sprintbootpractitionerv2.model.OfertaModel;
import grupo04hackathon.sprintbootpractitionerv2.service.repos.OfertaModelRepos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class OfertaModelServiceImpl implements OfertaModelService{

    @Autowired
    OfertaModelRepos ofertaModelRepos;

    public Page<OfertaModel> obtenerOfertaModels(int pagina, int tamanioPagina) {
        return this.ofertaModelRepos.findAll(PageRequest.of(pagina, tamanioPagina));
    }

}

package grupo04hackathon.sprintbootpractitionerv2.service;


import grupo04hackathon.sprintbootpractitionerv2.model.SolicitudModel;

import java.util.List;
import java.util.Optional;

public interface SolicitudService {

    public List<SolicitudModel> findAll();

    public SolicitudModel findByCodCliente(String codCliente);

    public void save(SolicitudModel solicitudModel);
    
    public Optional<SolicitudModel> findById(String idSolicitud);
    
    public void emparcharSolicitud(String idSolicitud, Double tasa, Double cuota, String estado);
}

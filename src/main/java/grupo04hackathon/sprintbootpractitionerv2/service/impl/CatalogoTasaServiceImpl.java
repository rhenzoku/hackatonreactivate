package grupo04hackathon.sprintbootpractitionerv2.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import grupo04hackathon.sprintbootpractitionerv2.model.CatalogoTasaModel;
import grupo04hackathon.sprintbootpractitionerv2.service.CatalogoTasaService;
import grupo04hackathon.sprintbootpractitionerv2.service.repo.CatalogoTasaRepository;

@Service
public class CatalogoTasaServiceImpl implements CatalogoTasaService {
	
	@Autowired CatalogoTasaRepository catalogoTasaRepository;

	@Override
	public List<CatalogoTasaModel> obtenerTasas() {
		return catalogoTasaRepository.findAll();
	}
	
	@Override
	public Optional<CatalogoTasaModel> obtenerTasaPorId(String id) {
		return catalogoTasaRepository.findById(id);
	}
	
	@Override
	public List<CatalogoTasaModel> obtenerTasaPorMontoPlazo(Double monto, Integer plazo){
		return catalogoTasaRepository.obtenerTasaPorMontoPlazo(monto, plazo);
	}

	@Override
	public String agregarTasas(CatalogoTasaModel catalogoTasa) {
		catalogoTasaRepository.insert(catalogoTasa);
		return catalogoTasa.getId();
	}

}

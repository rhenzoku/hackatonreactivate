package grupo04hackathon.sprintbootpractitionerv2.service.impl;

import grupo04hackathon.sprintbootpractitionerv2.model.ClienteModel;
import grupo04hackathon.sprintbootpractitionerv2.service.ClienteService;
import grupo04hackathon.sprintbootpractitionerv2.service.repo.ClienteRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteServiceImpl implements ClienteService {

    @Autowired
    ClienteRepositorio repositorioCliente;

    public Page<ClienteModel> obtenerClientes(int pagina, int tamPagina) {
        return this.repositorioCliente.findAll(PageRequest.of(pagina, tamPagina));
    }

    public void agregarCliente(ClienteModel cliente) {
        this.repositorioCliente.insert(cliente);
    }

    public ClienteModel obtenerCliente(String codCliente) {
        final Optional<ClienteModel> r = this.repositorioCliente.findById(codCliente);
        return r.isPresent() ? r.get() : null;
    }

    public ClienteModel obtenerClientePorDoc(String tipoDocumento, String nroDocumento){
        return this.repositorioCliente.obtenerClientePorDoc(tipoDocumento, nroDocumento);
    }

}

package grupo04hackathon.sprintbootpractitionerv2.service.impl;

import grupo04hackathon.sprintbootpractitionerv2.model.CronogramaModel;
import grupo04hackathon.sprintbootpractitionerv2.model.CuotaCronogramaModel;
import grupo04hackathon.sprintbootpractitionerv2.model.SolicitudModel;
import grupo04hackathon.sprintbootpractitionerv2.service.CuotaCronogramaService;
import grupo04hackathon.sprintbootpractitionerv2.service.repo.CuotaCronogramaServiceRepo;
import grupo04hackathon.sprintbootpractitionerv2.service.repo.SolicitudServiceRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CuotaCronogramaServiceImpl implements CuotaCronogramaService {

    @Autowired
    CuotaCronogramaServiceRepo cuotaCronogramaServiceRepo;

    public List<CuotaCronogramaModel> findAll() {
        return cuotaCronogramaServiceRepo.findAll();
    }

    public Optional<CuotaCronogramaModel> findById(String id) {
        return cuotaCronogramaServiceRepo.findById(id);
    }

    public void save(CuotaCronogramaModel cuotaCronogramaModel){
        cuotaCronogramaServiceRepo.save(cuotaCronogramaModel);
    }

    private Double pmt(Double rate,int term,Double financeAmount)
    {
        Double v = (1+(rate/12));
        Double t = (-(new Double(term)/12)*12);
        Double result=(financeAmount*(rate/12))/(1-Math.pow(v,t));
        return redondear(result);
    }

    private Double redondear(Double m){

        return Math.round(m*100.0)/100.0;

        //return truncateTo(m,2);
    }

    private Double tem(Double tea)
    {
       // Double tem =( Math.pow((1+tea),(1/12)) - new Double(1));
        Double tem = (1+(tea/12));
        return tem-1;
    }

    static Double truncateTo( Double unroundedNumber, int decimalPlaces ){
        int truncatedNumberInt = (int)( unroundedNumber * Math.pow( 10, decimalPlaces ) );
        Double truncatedNumber = (Double)( truncatedNumberInt / Math.pow( 10, decimalPlaces ) );
        return truncatedNumber;
    }

    public CuotaCronogramaModel calcularCronograma(Double rate,int term,Double financeAmount) {
        CuotaCronogramaModel cuotaCronogramaModel=new CuotaCronogramaModel();
        Double tem =tem(rate);
        Double cuota= pmt(rate,term,financeAmount);
        cuotaCronogramaModel.setCuota(redondear(cuota));
        List<CronogramaModel> cronograma=new ArrayList<CronogramaModel>();
        CronogramaModel cronogramaModel;
        Double financeAmountTemp=financeAmount;
        Double interes=new Double(0);
        Double amortizacion=new Double(0);
        for (int i = 1; i <= term; i++) {
            cronogramaModel=new CronogramaModel();
            cronogramaModel.setNroCuota(""+i);
            interes=financeAmountTemp*tem;
            cronogramaModel.setInteres(redondear(interes));
            amortizacion= cuota-interes;
            cronogramaModel.setAmortizacion(redondear(amortizacion));
            cronogramaModel.setCuota(redondear(interes+amortizacion));
            financeAmountTemp=financeAmountTemp-amortizacion;
            cronograma.add(cronogramaModel);
        }
        cuotaCronogramaModel.setCronograma(cronograma);
        return cuotaCronogramaModel;
    }



}

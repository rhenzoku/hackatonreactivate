package grupo04hackathon.sprintbootpractitionerv2.service.impl;

import grupo04hackathon.sprintbootpractitionerv2.model.DeudaClienteModel;
import grupo04hackathon.sprintbootpractitionerv2.service.DeudaClienteService;
import grupo04hackathon.sprintbootpractitionerv2.service.repos.RepositorioDeudaCliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;

import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DeudaClienteServiceImpl implements DeudaClienteService {

    @Autowired
    RepositorioDeudaCliente repositorioDeudaCliente;

    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public String agregarDeudaCliente(DeudaClienteModel dc){
        this.repositorioDeudaCliente.insert(dc);

        return dc.getCodCliente();
    }

    @Override
    public DeudaClienteModel obtenerDeudaCliente(String codCliente){

        System.out.println("impl - codCliente: "+codCliente);
        DeudaClienteModel deudaClienteModel = new DeudaClienteModel();
        final Query query=new Query();
        query.addCriteria(Criteria.where("codCliente").is(codCliente.trim()));
        System.out.println("codCliente:"+codCliente);
        return mongoTemplate.findOne(query,deudaClienteModel.getClass());
    }

    @Override
    public List<DeudaClienteModel> obtenerDeudaClientes(){
        return this.repositorioDeudaCliente.findAll();
    }
}


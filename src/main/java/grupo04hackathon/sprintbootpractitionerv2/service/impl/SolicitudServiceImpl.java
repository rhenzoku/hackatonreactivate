package grupo04hackathon.sprintbootpractitionerv2.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import grupo04hackathon.sprintbootpractitionerv2.model.SolicitudModel;
import grupo04hackathon.sprintbootpractitionerv2.service.SolicitudService;
import grupo04hackathon.sprintbootpractitionerv2.service.repo.SolicitudServiceRepo;

@Service
public class SolicitudServiceImpl implements SolicitudService {

    @Autowired
    SolicitudServiceRepo solicitudServiceRepo;

    public List<SolicitudModel> findAll() {
        return solicitudServiceRepo.findAll();
    }

    public SolicitudModel findByCodCliente(String  codCliente) {
        return solicitudServiceRepo.findByCodCliente(codCliente);
    }

    public void save(SolicitudModel solicitudModel){
        solicitudServiceRepo.save(solicitudModel);
    }
    
    public Optional<SolicitudModel> findById(String idSolicitud) {
    	return solicitudServiceRepo.findById(idSolicitud);
    }
    
    public void emparcharSolicitud(String idSolicitud, Double tasa, Double cuota, String estado) {
    	solicitudServiceRepo.emparcharSolicitud(idSolicitud, tasa, cuota, estado);
    }

}

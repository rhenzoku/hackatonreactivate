package grupo04hackathon.sprintbootpractitionerv2.service.repo;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import grupo04hackathon.sprintbootpractitionerv2.model.CatalogoTasaModel;

@Repository
public interface CatalogoTasaRepository extends MongoRepository<CatalogoTasaModel, String>, CatalogoTasaRepositoryPersonalizado{

}

package grupo04hackathon.sprintbootpractitionerv2.service.repo;

import java.util.List;

import org.springframework.stereotype.Repository;

import grupo04hackathon.sprintbootpractitionerv2.model.CatalogoTasaModel;

@Repository
public interface CatalogoTasaRepositoryPersonalizado {

	public List<CatalogoTasaModel> obtenerTasaPorMontoPlazo(Double monto, Integer plazo);
}

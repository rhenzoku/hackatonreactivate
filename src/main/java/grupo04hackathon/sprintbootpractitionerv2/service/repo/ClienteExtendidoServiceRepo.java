package grupo04hackathon.sprintbootpractitionerv2.service.repo;

import grupo04hackathon.sprintbootpractitionerv2.model.ClienteModel;
import grupo04hackathon.sprintbootpractitionerv2.model.SolicitudModel;
import org.springframework.stereotype.Repository;

@Repository
public interface ClienteExtendidoServiceRepo {
    public ClienteModel obtenerClientePorDoc (String tipoDocumento, String nroDocumento);
        }

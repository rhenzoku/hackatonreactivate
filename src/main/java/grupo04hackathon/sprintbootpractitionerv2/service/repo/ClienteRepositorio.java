package grupo04hackathon.sprintbootpractitionerv2.service.repo;

import grupo04hackathon.sprintbootpractitionerv2.model.ClienteModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClienteRepositorio extends MongoRepository<ClienteModel, String>,
ClienteExtendidoServiceRepo{
}

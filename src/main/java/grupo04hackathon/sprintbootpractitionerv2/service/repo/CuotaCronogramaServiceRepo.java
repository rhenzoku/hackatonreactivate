package grupo04hackathon.sprintbootpractitionerv2.service.repo;

import grupo04hackathon.sprintbootpractitionerv2.model.CuotaCronogramaModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CuotaCronogramaServiceRepo extends MongoRepository<CuotaCronogramaModel, String> { }



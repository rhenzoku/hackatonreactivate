package grupo04hackathon.sprintbootpractitionerv2.service.repo;

import grupo04hackathon.sprintbootpractitionerv2.model.SolicitudModel;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SolicitudExtendidaServiceRepo{
    public SolicitudModel findByCodCliente(String codCliente);
    public void emparcharSolicitud(String idSolicitud, Double tasa, Double cuota, String estado);
}

package grupo04hackathon.sprintbootpractitionerv2.service.repo;

import grupo04hackathon.sprintbootpractitionerv2.model.SolicitudModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SolicitudServiceRepo extends MongoRepository<SolicitudModel, String>, SolicitudExtendidaServiceRepo { }



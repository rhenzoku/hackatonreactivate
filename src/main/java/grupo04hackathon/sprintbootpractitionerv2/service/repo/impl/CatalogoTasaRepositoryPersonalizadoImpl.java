package grupo04hackathon.sprintbootpractitionerv2.service.repo.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import grupo04hackathon.sprintbootpractitionerv2.model.CatalogoTasaModel;
import grupo04hackathon.sprintbootpractitionerv2.service.repo.CatalogoTasaRepositoryPersonalizado;

public class CatalogoTasaRepositoryPersonalizadoImpl implements CatalogoTasaRepositoryPersonalizado {

	@Autowired
    MongoTemplate mongoTemplate;

	@Override
	public List<CatalogoTasaModel> obtenerTasaPorMontoPlazo(Double monto, Integer plazo) {
		
		final Query filtro = new Query();
		
		if(monto != null) {
			filtro.addCriteria(Criteria.where("importeMin").lt(monto));
	        filtro.addCriteria(Criteria.where("importeMax").gt(monto));
		}
        
        if(plazo != null) {
        	filtro.addCriteria(Criteria.where("plazoMin").lt(plazo));
            filtro.addCriteria(Criteria.where("plazoMax").gt(plazo));
        }
        
        return mongoTemplate.find(filtro, CatalogoTasaModel.class);
	}
	
	
}

package grupo04hackathon.sprintbootpractitionerv2.service.repo.impl;


import grupo04hackathon.sprintbootpractitionerv2.model.ClienteModel;
import grupo04hackathon.sprintbootpractitionerv2.model.SolicitudModel;
import grupo04hackathon.sprintbootpractitionerv2.service.repo.ClienteExtendidoServiceRepo;
import grupo04hackathon.sprintbootpractitionerv2.service.repo.SolicitudExtendidaServiceRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

public class ClienteExtendidoServiceRepoImpl implements ClienteExtendidoServiceRepo {

    @Autowired
    MongoTemplate mongoTemplate;

    public ClienteModel obtenerClientePorDoc(String tipoDocumento, String nroDocumento){
        ClienteModel clienteModel=new ClienteModel();
        final Query query=new Query();
        query.addCriteria(Criteria.where("tipoDocumento").is(tipoDocumento));
        query.addCriteria(Criteria.where("nroDocumento").is(nroDocumento));

        return mongoTemplate.findOne(query,clienteModel.getClass());
    }


}

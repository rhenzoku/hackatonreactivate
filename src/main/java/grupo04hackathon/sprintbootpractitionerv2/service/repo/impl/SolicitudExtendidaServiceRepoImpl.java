package grupo04hackathon.sprintbootpractitionerv2.service.repo.impl;


import grupo04hackathon.sprintbootpractitionerv2.model.SolicitudModel;
import grupo04hackathon.sprintbootpractitionerv2.service.repo.SolicitudExtendidaServiceRepo;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.util.Optional;

public class SolicitudExtendidaServiceRepoImpl implements SolicitudExtendidaServiceRepo {

    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public SolicitudModel findByCodCliente(String codcliente){
        SolicitudModel solicitudModel=new SolicitudModel();
        final Query query=new Query();
        query.addCriteria(Criteria.where("clienteModel.codCliente").is(codcliente));
        query.addCriteria(Criteria.where("estado").is("0"));

        return mongoTemplate.findOne(query,solicitudModel.getClass());
    }

    @Override
    public void emparcharSolicitud(String idSolicitud, Double tasa, Double cuota, String estado) {

        final Query filtro = new Query();
        filtro.addCriteria(Criteria.where("_id").is(new ObjectId(idSolicitud)));

        final Update parche = new Update();
        if(tasa != null) parche.set("tasa", tasa);
        if(cuota != null) parche.set("cuota", cuota);
        if(estado != null) parche.set("estado", estado);

        this.mongoTemplate.updateFirst(filtro, parche, "solicitudModel");
    }
}

package grupo04hackathon.sprintbootpractitionerv2.service.repos;

import grupo04hackathon.sprintbootpractitionerv2.model.OfertaModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OfertaModelRepos extends MongoRepository<OfertaModel, String>,
    OfertaModelReposExtendido{
}

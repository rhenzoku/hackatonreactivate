package grupo04hackathon.sprintbootpractitionerv2.service.repos;

import grupo04hackathon.sprintbootpractitionerv2.model.DeudaClienteModel;
import grupo04hackathon.sprintbootpractitionerv2.service.DeudaClienteService;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepositorioDeudaCliente extends MongoRepository<DeudaClienteModel, String> {
}
